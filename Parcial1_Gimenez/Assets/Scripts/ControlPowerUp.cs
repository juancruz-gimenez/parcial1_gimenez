using UnityEngine;

public class ControlPowerUp : MonoBehaviour
{
    public GameObject explosion;
    public GameObject PowerUp;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") == true)
        {
            Destroy(PowerUp);
            GameObject particulas = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
            Destroy(particulas, 2);

        }
    }
}

