using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public TMPro.TMP_Text textoObjetosRecolectados;
    public TMPro.TMP_Text textoGanaste;
    private int cont;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private Rigidbody rb;
    public bool aire = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        cont = 0;
        textoGanaste.text = "";
        setearTextos();
        Cursor.lockState = CursorLockMode.Locked;
        GestorDeAudio.instancia.ReproducirSonido("musica");
    }

    private void setearTextos()
    {
        textoObjetosRecolectados.text = "Cantidad recolectados: " + cont.ToString();
        if (cont >= 4)
        {
            textoGanaste.text = "Ganaste!";
        }
    }
    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            aire = true;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && aire == true)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            aire = false;
        }

    }


    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
            GestorDeAudio.instancia.ReproducirSonido("monedita");
        }

        if (other.gameObject.CompareTag("PowerUp") == true)
        {
            rapidezDesplazamiento = rapidezDesplazamiento + 3;
            transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);

        }
    }
}



