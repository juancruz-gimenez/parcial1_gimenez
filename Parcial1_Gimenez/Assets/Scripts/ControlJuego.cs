using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{
    public GameObject Jugador;
    float tiempoRestante;
    public TMPro.TMP_Text textoCronometro;
    public TMPro.TMP_Text textoGameOver;

    public void RestartGame()
    {
        SceneManager.LoadScene("Parcial1");
    }
    void Start()
    {
        StartCoroutine(ComenzarCronometro(60));

    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartGame();
        }

        if (Jugador.transform.position.y <= -5f)
        {
            RestartGame();

        }
        if (tiempoRestante == 0)
        {
            textoGameOver.text = "Game Over";
            Time.timeScale = 0;
        }
    }

        public IEnumerator ComenzarCronometro(float valorCronometro = 60)
        {
            tiempoRestante = valorCronometro;
            while (tiempoRestante > 0)
            {
                Debug.Log("Restan " + tiempoRestante + " segundos.");
                yield return new WaitForSeconds(1.0f);
                tiempoRestante--;
                textoCronometro.text = "Tiempo Restante: " + tiempoRestante;
            }
            
        }
    

}



