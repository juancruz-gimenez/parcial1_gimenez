using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GameManager : MonoBehaviour
{
    private void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("musica");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            GestorDeAudio.instancia.ReproducirSonido("monedita");
        }
    }
}
