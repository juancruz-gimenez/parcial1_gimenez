using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoAplastador : MonoBehaviour
{

    bool tengoQueBajar = false;
    float rapidez = 1.2f;

    void Update()
    {
        if (transform.position.y >= 5)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= 1.5)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * 8 * Time.deltaTime;
    }
}
