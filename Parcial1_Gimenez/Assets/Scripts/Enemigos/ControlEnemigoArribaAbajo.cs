using UnityEngine;

public class ControlEnemigoArribaAbajo : MonoBehaviour
{

    bool tengoQueBajar = false;
    int rapidez = 3;

    void Update()
    {
        if (transform.position.y >= 3)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= -0.5f)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
