using UnityEngine;

public class ControlEnemigoAdelAtras : MonoBehaviour
{ 

    bool IrAdelante = false;
    int rapidez = 3;

    void Update()
    {
        if (transform.position.z >= -2.25f)
        {
            IrAdelante = true;
        }
        if (transform.position.z <= -7.5f)
        {
            IrAdelante = false;
        }

        if (IrAdelante)
        {
            Adelante();
        }
        else
        {
            Atras();
        }

    }

    void Adelante()
    {
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    }

    void Atras()
    {
        transform.position += transform.forward * rapidez * Time.deltaTime;
    }
}