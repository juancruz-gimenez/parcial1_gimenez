using UnityEngine;

public class ControlEnemigoIzqDer : MonoBehaviour
{

    bool IrIzquierda = false;
    int rapidez = 3;

    void Update()
    {
        if (transform.position.x >= -15)
        {
            IrIzquierda = true;
        }
        if (transform.position.x <= -19)
        {
            IrIzquierda = false;
        }

        if (IrIzquierda)
        {
            Izquierda();
        }
        else
        {
            Derecha();
        }

    }

    void Izquierda ()
    {
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    }

    void Derecha()
    {
        transform.position += transform.forward * rapidez * Time.deltaTime;
    }
}
